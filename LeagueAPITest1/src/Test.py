from champrequest import ChampionRequest as CR
import pprint

printer = pprint.PrettyPrinter(indent = 4)

myrequest = CR(printerindent = 2)
toprint = myrequest.getcurrentchampions(isfree = False)
printer.pprint(toprint)
