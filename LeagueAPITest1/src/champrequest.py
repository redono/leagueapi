import simplejson as json
import urllib.request as request
from urllib.error import HTTPError, URLError
import pprint

class ChampionRequest:
    """A simple little requesthandler for champions. Might make this into something more to use for myself, who knows :D """
    def __init__(self, riotapikey = "4af7b974-e29e-4c9f-b3b4-d09e01e486a3", printerindent = 4):
        self.url = "http://prod.api.pvp.net/api/lol/euw/v1.1/champion?freeToPlay="
        self.riotapikey = riotapikey
        self.printer = pprint.PrettyPrinter(printerindent)
        
    def set_url(self, url):
        self.url = url   
    
    def get_url(self):
        return self.url
    
    def getcurrentchampions(self, isfree = True):
        if isfree:
            championrequest = request.Request(self.url+"true&api_key="+self.riotapikey)
        else:
            championrequest = request.Request(self.url+"false&api_key="+self.riotapikey)
        
        try:
            response = request.urlopen(championrequest)
            
        except URLError as err:
            print("The url could not be retrieved")
            print(err.reason)
            return -1
        
        except HTTPError as err:
            print("There was an error in the HTTP-request")
            print(err.code)
            print(err.read())
            
        else:
            return json.load(response)